﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pokemon_Manager.Service;
using Pokemon_Manager.Model;

namespace Pokemon_Manager.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            IPokemonService pservice = new PokemonService();

            while (true)
            {
                foreach (Pokemon pokemon in pservice.GetPokemonLikeIdentifier(System.Console.ReadLine(), true))
                {
                    System.Console.WriteLine(pokemon.Name + " " + pokemon.Typ1 + " " + pokemon.Typ2);
                }
                System.Console.WriteLine();
            }
        }
    }
}
