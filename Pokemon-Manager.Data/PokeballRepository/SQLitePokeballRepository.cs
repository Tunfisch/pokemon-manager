﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Data
{
    public class SQLitePokeballRepository : IPokeballRepository
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ISqlConnector sqlconnector = DatabaseRepository.SQLConnector;

        public List<Model.Pokeball> GetAllPokeballs()
        {
            string sqlCommand = "SELECT * FROM Pokeballs;";

            DbDataReader reader = sqlconnector.ExecuteReader(sqlCommand);

            List<Model.Pokeball> result = new List<Model.Pokeball>();

            if (reader != null)
            {
                try
                {
                    while (reader.Read())
                    {
                        result.Add(new Model.Pokeball(reader["name"].ToString(), float.Parse(reader["catchrate"].ToString())));
                    }
                }
                catch (DbException ex)
                {
                    log.Debug(ex.Message, ex);
                }
                reader.Close();
            }
            else
            {
                log.Debug("READER IST NULL");
            }

            return result;
        }
    }
}
