﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Data
{
    public interface ISqlConnector
    {
        DbConnection SqlConnection { get; }
        DbTransaction SqlTransaction { get; }
        DATABASE ReadIniFile();
        DbConnection GetDbCon(string connectionString);
        bool ExecuteNonQuery(params string[] queries);
        bool ExecuteNonQuery(List<string> queries);
        bool ExecuteNonQuerySqlFile(string file);
        DbDataReader ExecuteReader(string commandText);
        void StartTransaction();
        void StopTransaction();
        void RollBackTransaction();
        bool OpenDatabase();
        bool CloseDatabase();
    }
}
