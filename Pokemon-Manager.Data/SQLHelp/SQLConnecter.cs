﻿using IniParser;
using IniParser.Model;
using log4net;
using MySql.Data.MySqlClient;
using Pokemon_Manager.Data.SQLHelp;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Data
{
    public class SQLConnecter : ISqlConnector
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private DbConnection dbCon;
        private DbCommand dbCommand;
        private DbDataReader dbDataReader;
        private DbTransaction dbTransaction;
        private DATABASE currentSqlDataBase;
        private static string datainifilepath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "data.ini";

        public SQLConnecter(string connectionString, DATABASE database)
        {
            currentSqlDataBase = ReadIniFile();
            dbCon = GetDbCon(connectionString);
            dbCommand = dbCon.CreateCommand();
            OpenDatabase();               
        }

        public DbConnection SqlConnection
        {
            get { return dbCon; }
        }

        public DbTransaction SqlTransaction
        {
            get { return dbTransaction; }
        }

        public DATABASE ReadIniFile()
        {
            FileIniDataParser fileini = new IniParser.FileIniDataParser();

            IniData data = fileini.ReadFile(datainifilepath);

            if (data["Databasetyp"]["Datafrom"].ToUpper() == "MSSQL")
            {
                return DATABASE.MSSQL;
            }
            else if (data["Databasetyp"]["Datafrom"].ToUpper() == "MYSQL")
            {
                return DATABASE.MySql;
            }
            else if(data["Databasetyp"]["Datafrom"].ToUpper() == "ORACLE")
            {
                return DATABASE.Oracle;
            }
            else if(data["Databasetyp"]["Datafrom"].ToUpper() == "POSTGRESQL")
            {
                return DATABASE.PostgreSQL;
            }
            else if(data["Databasetyp"]["Datafrom"].ToUpper() == "SQLITE")
            {
                return DATABASE.SQLite;
            }
            else
            {
                log.Info("INI data dont have a valid Database Input [DATAFROM] Parameter");
                throw new SQLNotDeclaredException();
            }
        }

        public DbConnection GetDbCon(string connectionString)
        {
            switch (currentSqlDataBase)
            {
                case DATABASE.MySql:
                    return new MySqlConnection(connectionString);
                case DATABASE.SQLite:
                    return new SQLiteConnection(connectionString);
                default:
                    throw new SQLNotDeclaredException();
            }
        }

        public bool ExecuteNonQuery(params string[] queries)
        {
            if (dbCon.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    foreach (string query in queries)
                    {
                        if (query != "")
                        {
                            dbCommand.CommandText = query;
                            dbCommand.ExecuteNonQuery();
                        }
                    }
                    return true;
                }
                catch (DbException ex)
                {
                    log.Error("Error in Method SQLConnector.ExecuteNonQuery(params string[] queries)", ex);
                    return false;
                }
            }
            else
            {
                log.Info("Datenbank ist nicht geöffnet");
                return false;
            }
        }

        public DbDataReader ExecuteReader(string commandText)
        {
            if (dbCon.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    dbCommand.CommandText = commandText;
                    
                    dbDataReader = dbCommand.ExecuteReader();

                    return dbDataReader;
                }
                catch (Exception ex)
                {
                    log.Error("Error in Method SQLConnector.ExecuteReader(string commandText)", ex);
                    return null;
                }
            }
            else
            {
                log.Info("Datenbank ist nicht geöffnet");
                return null;
            }
        }

        public bool ExecuteNonQuery(List<string> queries)
        {
            if (dbCon.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    foreach (string query in queries)
                    {
                        if (query != "")
                        {
                            dbCommand.CommandText = query;
                            dbCommand.ExecuteNonQuery();
                        }
                    }
                    return true;
                }
                catch (DbException ex)
                {
                    log.Error("Error in Method SQLConnector.ExecuteNonQuery(List<string> queries)", ex);
                    return false;
                }
            }
            else
            {
                log.Info("Datenbank ist nicht geöffnet");
                return false;
            }
        }

        public bool ExecuteNonQuerySqlFile(string file)
        {
            List<string> allQueries = new List<string>();

            if (dbCon.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    using (StreamReader reader = new StreamReader(file))
                    {
                        string data = reader.ReadToEnd();
                        string[] tmp = data.Split(';');
                        allQueries = tmp.ToList<string>();
                    }
                }
                catch(Exception ex)
                {
                    log.Error("Error in Method SQLConnector.ExecuteNonQuerySqlFile(string file)", ex);
                    return false;
                }
            }
            else
            {
                log.Info("Datenbank ist nicht geöffnet");
                return false;
            }

            return ExecuteNonQuery(allQueries);
        }

        public void StartTransaction()
        {
            dbTransaction = dbCon.BeginTransaction();
        }

        public void StopTransaction()
        {
            if(dbTransaction != null)
                dbTransaction.Commit();
        }

        public void RollBackTransaction()
        {
            if(dbTransaction != null)
                dbTransaction.Rollback();
        }

        public bool OpenDatabase()
        {
            if (dbCon.State == System.Data.ConnectionState.Closed)
            {
                try
                {
                    dbCon.Open();
                    log.Info("Datenbank wurde geöffnet " + DateTime.Now);
                    return true;
                }
                catch (DbException ex)
                {
                    log.Info("Datenbank konnte nicht geöffnet werden " + DateTime.Now, ex);
                    return false;
                }
            }
            else
            {
                log.Info("Datenbank ist schon geöffnet " + DateTime.Now);
                return false;
            }
        }

        public bool CloseDatabase()
        {
            if (dbCon.State == System.Data.ConnectionState.Open)
            {
                try
                {
                    dbCon.Close();
                    log.Info("Datenbank wurde geschlossen" + DateTime.Now);
                    return true;
                }
                catch (Exception ex)
                {
                    log.Error("Datenbank konnte nicht geschlossen werden" + DateTime.Now, ex);
                    return false;
                }
            }
            else
            {
                log.Info("Datenbank ist schon geschlossen " + DateTime.Now);
                return false;
            }
        }
    }
}
