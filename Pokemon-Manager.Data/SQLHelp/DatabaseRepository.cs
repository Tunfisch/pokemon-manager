﻿using IniParser;
using IniParser.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Data
{
    public enum DATABASE { MySql, MSSQL, Oracle, PostgreSQL, SQLite, XML };

    public class DatabaseRepository
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static DATABASE currentDatabase = DATABASE.SQLite;
        private static ISqlConnector sqlConnector = new SQLConnecter(BuildConnectionString(), currentDatabase);

        public static DATABASE DataBase { get {return currentDatabase; } }
        public static ISqlConnector SQLConnector{get {return sqlConnector; } }

        public static IPokemonRepository GetPokemonRepository()
        {
            switch (currentDatabase)
            {
                case DATABASE.MySql:
                    return new MySqlPokemonRepository();
                case DATABASE.SQLite:
                    return new SQLitePokemonRepository();
                case DATABASE.XML:
                    return null;
                default:
                    return null;
            }
        }

        public static IPokeballRepository GetPokeballRepository()
        {
            switch (currentDatabase)
            {
                case DATABASE.MySql:
                    return new MySqlPokeballRepository();
                case DATABASE.SQLite:
                    return new SQLitePokeballRepository();
                case DATABASE.XML:
                    return null;
                default:
                    return null;
            }
        }

        private static string BuildConnectionString()
        {
            var parser = new FileIniDataParser();
            IniData data = null;

            try
            {
                data = parser.ReadFile(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "data.ini");
            }
            catch(Exception ex)
            {
                log.Error("data.ini konnte nicht geparsed werden", ex);
                return "";
            }

            switch(currentDatabase)
            {
                case DATABASE.MySql:
                    try
                    {
                        string server = data["Databasetyp"]["server"];
                        string database = data["Databasetyp"]["database"];
                        string user = data["Databasetyp"]["user"];
                        string pass = data["Databasetyp"]["pass"];
                        string port = data["Databasetyp"]["port"];
                        
                        return "Server="+server+";Database="+database+";Uid="+user+";Pwd="+pass+";Port="+port+";";
                    }
                    catch(Exception ex)
                    {
                        log.Error("MySQL data.ini couldnt read", ex);
                        return "";
                    }
                case DATABASE.SQLite:
                    try
                    {
                        string database = data["Databasetyp"]["database"];
                        string version = data["Databasetyp"]["version"];
                        //string pass = data["Databasetyp"]["pass"];

                        return "Data Source="+database+";Version="+version+";";//Password="+pass+";";
                    }
                    catch(Exception ex)
                    {
                        log.Error("SQLite data.ini couldnt read", ex);
                        return "";
                    }
                default:
                    return "";
            }
            
        }
    }
}
