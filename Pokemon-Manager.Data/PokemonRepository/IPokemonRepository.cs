﻿using Pokemon_Manager.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Data
{
    public interface IPokemonRepository
    {
        List<Pokemon> GetAllPokemon();
        List<Pokemon> GetPokemonById(int id);
        List<Pokemon> GetPokemonLikeIdentifier(string identifier);
        List<Pokemon> GetPokemonLikeIdentifierAndId(string identifier, int id);
        List<Model.Pokemon> ReadPokemonData(string sqlCommand);
        DataTable GetDataTable(List<Pokemon> pokemonList);
        void UpdatePokemon(Pokemon pokemon);
    }
}
