﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Data.PokemonRepository
{
    class XMLPokemonRepository : IPokemonRepository
    {
        public List<Model.Pokemon> GetAllPokemon()
        {
            throw new NotImplementedException();
        }

        public List<Model.Pokemon> GetPokemonById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifier(string identifier)
        {
            throw new NotImplementedException();
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifierAndId(string identifier, int id)
        {
            throw new NotImplementedException();
        }

        public void UpdatePokemon(Model.Pokemon pokemon)
        {
            throw new NotImplementedException();
        }

        public System.Data.DataTable GetDataTable(List<Model.Pokemon> pokemonList)
        {
            throw new NotImplementedException();
        }


        public List<Model.Pokemon> ReadPokemonData(string sqlCommand)
        {
            throw new NotImplementedException();
        }
    }
}
