﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Data
{
    /// <summary>
    /// Data Object für Model.Pokemon
    /// </summary>
    public class MySqlPokemonRepository : IPokemonRepository
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ISqlConnector sqlconnector = DatabaseRepository.SQLConnector;
        

        public List<Model.Pokemon> ReadPokemonData(string sqlCommand)
        {
            DbDataReader reader = sqlconnector.ExecuteReader(sqlCommand);

            List<Model.Pokemon> result = new List<Model.Pokemon>();

            if (reader != null)
            {
                try
                {
                    while (reader.Read())
                    {
                        List<string> typ12 = reader["Typ"].ToString().Split(',').ToList<string>();
                        if (typ12.Count <= 1)
                            typ12.Add("");

                        result.Add(new Model.Pokemon((int)reader["id"], reader["identifier"].ToString(), typ12[0], typ12[1], Convert.ToBoolean(reader["catch"]), Convert.ToInt32(reader["capture_rate"])));
                    }
                }
                catch (DbException ex)
                {
                    log.Debug(ex.Message, ex);
                }
                reader.Close();
            }
            else
            {
                log.Debug("READER IST NULL");
            }

            return result;
        }

        public List<Model.Pokemon> GetAllPokemon()
        {
            string SelectCommand = @"SELECT        pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.order, pokemon.is_default, 
                         pokemon.catch, GROUP_CONCAT(types.identifier ORDER BY slot) AS Typ
FROM            pokemon INNER JOIN
                         pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
                         types ON pokemon_types.type_id = types.id
GROUP BY pokemon.id";

            return ReadPokemonData(SelectCommand);
        }

        public List<Model.Pokemon> GetPokemonById(int id)
        {
            string SelectCommand = @"SELECT        pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.order, pokemon.is_default, 
                         pokemon.catch, GROUP_CONCAT(types.identifier ORDER BY slot) AS Typ
FROM            pokemon INNER JOIN
                         pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
                         types ON pokemon_types.type_id = types.id
GROUP BY pokemon.id
HAVING pokemon.id='" + id.ToString() + "';";

            return ReadPokemonData(SelectCommand);
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifier(string identifier)
        {
            string SelectCommand = @"SELECT        pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.order, pokemon.is_default, 
                         pokemon.catch, GROUP_CONCAT(types.identifier ORDER BY slot) AS Typ
FROM            pokemon INNER JOIN
                         pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
                         types ON pokemon_types.type_id = types.id
GROUP BY pokemon.id
HAVING pokemon.identifier LIKE '" + identifier + "%';";

            return ReadPokemonData(SelectCommand);
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifierAndId(string identifier, int id)
        {
            string SelectCommand;

            SelectCommand = @"SELECT        pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.order, pokemon.is_default, 
                         pokemon.catch, GROUP_CONCAT(types.identifier ORDER BY slot) AS Typ
FROM            pokemon INNER JOIN
                         pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
                         types ON pokemon_types.type_id = types.id
GROUP BY pokemon.id
HAVING pokemon.identifier LIKE '" + identifier + "%' AND pokemon.id='"+id.ToString()+"';";

            return ReadPokemonData(SelectCommand);
        }

        public DataTable GetDataTable(List<Model.Pokemon> pokemonList)
        {
            System.Data.DataTable table = new System.Data.DataTable("Pokemon");
            
            table.Columns.Add("id", typeof(int));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Typ1", typeof(string));
            table.Columns.Add("Typ2", typeof(string));
            table.Columns.Add("Catch", typeof(bool));

            foreach(Model.Pokemon pokemon in pokemonList)
            {
                table.Rows.Add(pokemon.Id, pokemon.Name, pokemon.Typ1, pokemon.Typ2, pokemon.Catch);
            }

            return table;
        }

        public void UpdatePokemon(Model.Pokemon pokemon)
        {
            //NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE
            //NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE
            //NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE
            //NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE

            string insertCommand = @"UPDATE pokemon SET id='"+pokemon.Id+"', identifier='"+pokemon.Name+"', catch='"+Convert.ToInt32(pokemon.Catch)+"' WHERE pokemon.id='"+pokemon.Id+"';";

            sqlconnector.ExecuteNonQuery(insertCommand);
        }
    }
}
