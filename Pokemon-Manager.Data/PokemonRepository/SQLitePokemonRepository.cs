﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Data
{
    public class SQLitePokemonRepository : IPokemonRepository
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ISqlConnector sqlconnector = DatabaseRepository.SQLConnector;

        public List<Model.Pokemon> ReadPokemonData(string sqlCommand)
        {
            List<Model.Pokemon> result = new List<Model.Pokemon>();
            DbDataReader reader;
            try
            {
                reader = sqlconnector.ExecuteReader(sqlCommand);

            }
            catch(DbException ex)
            {
                log.Error("Reader konnte nicht ausgeführt werden", ex);
                return result;
            }

            if (reader != null)
            {
                try
                {
                    while (reader.Read())
                    {
                        List<string> typ12 = reader["Typ"].ToString().Split('2').ToList<string>();
                        if (typ12.Count <= 1)
                            typ12.Add("");

                        result.Add(new Model.Pokemon(Convert.ToInt32(reader["id"]), reader["identifier"].ToString(), typ12[0], typ12[1], Convert.ToBoolean(reader["catch"]), Convert.ToInt32(reader["capture_rate"])));
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.Message, ex);
                }
                finally
                {
                    reader.Close();
                }
            }
            else
            {
                log.Error("READER IST NULL");
            }

            return result;
        }

        public List<Model.Pokemon> GetAllPokemon()
        {
            string SelectCommand = @"SELECT pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.is_default,
pokemon.catch, pokemon_species.capture_rate, GROUP_CONCAT(types.identifier, pokemon_types.slot) AS Typ
FROM pokemon INNER JOIN
pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
types ON pokemon_types.type_id = types.id 
INNER JOIN pokemon_species ON pokemon.id = pokemon_species.id
GROUP BY pokemon.id";

            return ReadPokemonData(SelectCommand);
        }

        public List<Model.Pokemon> GetPokemonById(int id)
        {
            string SelectCommand = @"SELECT pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.is_default,
pokemon.catch, pokemon_species.capture_rate, GROUP_CONCAT(types.identifier, pokemon_types.slot) AS Typ
FROM pokemon INNER JOIN
pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
types ON pokemon_types.type_id = types.id 
INNER JOIN pokemon_species ON pokemon.id = pokemon_species.id
GROUP BY pokemon.id 
HAVING pokemon.id='" + id.ToString() + "';";

            return ReadPokemonData(SelectCommand);
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifier(string identifier)
        {
            string SelectCommand = @"SELECT pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.is_default,
pokemon.catch, pokemon_species.capture_rate, GROUP_CONCAT(types.identifier, pokemon_types.slot) AS Typ
FROM pokemon INNER JOIN
pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
types ON pokemon_types.type_id = types.id 
INNER JOIN pokemon_species ON pokemon.id = pokemon_species.id
GROUP BY pokemon.id 
HAVING pokemon.identifier LIKE '" + identifier + "%';";

            return ReadPokemonData(SelectCommand);
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifierAndId(string identifier, int id)
        {
            string SelectCommand;

            SelectCommand = @"SELECT pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.is_default,
pokemon.catch, pokemon_species.capture_rate, GROUP_CONCAT(types.identifier, pokemon_types.slot) AS Typ
FROM pokemon INNER JOIN
pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
types ON pokemon_types.type_id = types.id 
INNER JOIN pokemon_species ON pokemon.id = pokemon_species.id
GROUP BY pokemon.id 
HAVING pokemon.identifier LIKE '" + identifier + "%' AND pokemon.id='"+id.ToString()+"';";

            return ReadPokemonData(SelectCommand);
        }

        public DataTable GetDataTable(List<Model.Pokemon> pokemonList)
        {
            System.Data.DataTable table = new System.Data.DataTable("Pokemon");
            
            table.Columns.Add("id", typeof(int));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Typ1", typeof(string));
            table.Columns.Add("Typ2", typeof(string));
            table.Columns.Add("Catch", typeof(bool));

            foreach(Model.Pokemon pokemon in pokemonList)
            {
                table.Rows.Add(pokemon.Id, pokemon.Name, pokemon.Typ1, pokemon.Typ2, pokemon.Catch);
            }

            return table;
        }

        public void UpdatePokemon(Model.Pokemon pokemon)
        {
            //NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE
            //NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE
            //NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE
            //NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE NOT COMPLETE

            string insertCommand = @"UPDATE pokemon SET id='"+pokemon.Id+"', identifier='"+pokemon.Name+"', catch='"+Convert.ToInt32(pokemon.Catch)+"' WHERE pokemon.id='"+pokemon.Id+"';";

            sqlconnector.ExecuteNonQuery(insertCommand);
        }

        #region as
        /*public List<Model.Pokemon> GetAllPokemon()
        {
            string SelectCommand = @"SELECT        pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.is_default, 
                         pokemon.catch, GROUP_CONCAT(types.identifier, pokemon_types.slot) AS Typ
FROM            pokemon INNER JOIN
                         pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
                         types ON pokemon_types.type_id = types.id
GROUP BY pokemon.id";

            DbDataReader reader = sqlconnector.ExecuteReader(SelectCommand);

            List<Model.Pokemon> result = new List<Model.Pokemon>();

            if (reader != null)
            {
                try
                {
                    while (reader.Read())
                    {
                        string[] typ12 = reader["Typ"].ToString().Split(',');
                        if (typ12.Length > 1)
                            result.Add(new Model.Pokemon(Convert.ToInt32(reader["id"]), reader["identifier"].ToString(), typ12[0], typ12[1], Convert.ToBoolean(reader["catch"])));
                        else
                            result.Add(new Model.Pokemon(Convert.ToInt32(reader["id"]), reader["identifier"].ToString(), typ12[0], "", Convert.ToBoolean(reader["catch"])));
                    }
                }
                catch (DbException)
                {

                }
                reader.Close();
            }

            return result;
        }

        public List<Model.Pokemon> GetPokemonById(int id)
        {
            string SelectCommand = @"SELECT        pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.order, pokemon.is_default, 
                         pokemon.catch, GROUP_CONCAT(types.identifier ORDER BY slot) AS Typ
FROM            pokemon INNER JOIN
                         pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
                         types ON pokemon_types.type_id = types.id
GROUP BY pokemon.id
HAVING pokemon.id='" + id.ToString() + "';";

            DbDataReader reader = sqlconnector.ExecuteReader(SelectCommand);

            List<Model.Pokemon> result = new List<Model.Pokemon>();

            if (reader != null)
            {
                try
                {
                    while (reader.Read())
                    {
                        string[] typ12 = reader["Typ"].ToString().Split(',');
                        if (typ12.Length > 1)
                            result.Add(new Model.Pokemon((int)reader["id"], reader["identifier"].ToString(), typ12[0], typ12[1], Convert.ToBoolean(reader["catch"])));
                        else
                            result.Add(new Model.Pokemon((int)reader["id"], reader["identifier"].ToString(), typ12[0], "", Convert.ToBoolean(reader["catch"])));
                    }
                }
                catch (DbException ex)
                {

                }
                reader.Close();
            }

            return result;
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifier(string identifier)
        {
            string SelectCommand = @"SELECT        pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.order, pokemon.is_default, 
                         pokemon.catch, GROUP_CONCAT(types.identifier ORDER BY slot) AS Typ
FROM            pokemon INNER JOIN
                         pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
                         types ON pokemon_types.type_id = types.id
GROUP BY pokemon.id
HAVING pokemon.identifier LIKE '" + identifier + "%';";

            DbDataReader reader = sqlconnector.ExecuteReader(SelectCommand);

            List<Model.Pokemon> result = new List<Model.Pokemon>();

            if (reader != null)
            {
                try
                {
                    while (reader.Read())
                    {
                        string[] typ12 = reader["Typ"].ToString().Split(',');
                        if (typ12.Length > 1)
                            result.Add(new Model.Pokemon((int)reader["id"], reader["identifier"].ToString(), typ12[0], typ12[1], Convert.ToBoolean(reader["catch"])));
                        else
                            result.Add(new Model.Pokemon((int)reader["id"], reader["identifier"].ToString(), typ12[0], "", Convert.ToBoolean(reader["catch"])));
                    }
                }
                catch (DbException ex)
                {

                }
                reader.Close();
            }

            return result;
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifierAndId(string identifier, int id)
        {
            string SelectCommand;

            SelectCommand = @"SELECT        pokemon.id, pokemon.identifier, pokemon.species_id, pokemon.height, pokemon.weight, pokemon.base_experience, pokemon.order, pokemon.is_default, 
                         pokemon.catch, GROUP_CONCAT(types.identifier ORDER BY slot) AS Typ
FROM            pokemon INNER JOIN
                         pokemon_types ON pokemon.id = pokemon_types.pokemon_id INNER JOIN
                         types ON pokemon_types.type_id = types.id
GROUP BY pokemon.id
HAVING pokemon.identifier LIKE '" + identifier + "%' AND pokemon.id='" + id.ToString() + "';";

            DbDataReader reader = sqlconnector.ExecuteReader(SelectCommand);

            List<Model.Pokemon> result = new List<Model.Pokemon>();

            if (reader != null)
            {
                try
                {
                    while (reader.Read())
                    {
                        string[] typ12 = reader["Typ"].ToString().Split(',');
                        if (typ12.Length > 1)
                            result.Add(new Model.Pokemon((int)reader["id"], reader["identifier"].ToString(), typ12[0], typ12[1], Convert.ToBoolean(reader["catch"])));
                        else
                            result.Add(new Model.Pokemon((int)reader["id"], reader["identifier"].ToString(), typ12[0], "", Convert.ToBoolean(reader["catch"])));
                    }
                }
                catch (DbException ex)
                {

                }
                reader.Close();
            }

            return result;
        }

        public DataTable GetDataTable(List<Model.Pokemon> pokemonList)
        {
            System.Data.DataTable table = new System.Data.DataTable("Pokemon");

            table.Columns.Add("id", typeof(int));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Typ1", typeof(string));
            table.Columns.Add("Typ2", typeof(string));

            foreach (Model.Pokemon pokemon in pokemonList)
            {
                table.Rows.Add(pokemon.Id, pokemon.Name, pokemon.Typ1, pokemon.Typ2);
            }

            return table;
        }

        public void UpdatePokemon(Model.Pokemon pokemon)
        {
            string insertCommand = @"UPDATE pokemon SET id='" + pokemon.Id + "' identifier='" + pokemon.Name + "' catch='" + pokemon.Id + "';";

            sqlconnector.ExecuteNonQuery(insertCommand);
        }


        public List<Model.Pokemon> ReadPokemonData(string sqlCommand)
        {
            throw new NotImplementedException();
        }*/
        #endregion
    }
}
