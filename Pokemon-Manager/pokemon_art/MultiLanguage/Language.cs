﻿using Pokemon_Manager.pokemon_art.MultiLanguage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pokemon_Manager.MultiLanguage
{
    class Language : Strings
    {
        private static string cultureString;

        public Language(string culture)
        {
            cultureString = culture;
            InitializeLocalisation();
        }

        static void InitializeLocalisation()
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(cultureString);
        }
    }
}
