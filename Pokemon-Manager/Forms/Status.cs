﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Forms
{
    class Status
    {
        private string status_name;
        private float status_rate;

        public string Name { get { return status_name; } set { status_name = value; } }
        public float Rate { get { return status_rate; } set { status_rate = value; } }

        public Status(string name, float catchrate)
        {
            status_name = name;
            status_rate = catchrate;
        }

        public override string ToString()
        {
            return status_name;
        }
    }
}
