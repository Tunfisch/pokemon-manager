﻿using log4net;
using Pokemon_Manager.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pokemon_Manager.Forms
{
    public partial class Fangchance : Form
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static IPokemonService psrv = new PokemonService();
        private static IPokeballService pokeballService = new PokeballService();
        private static List<Model.Pokemon> listPokemon;
        private static List<Model.Pokeball> allPokeBalls = new List<Model.Pokeball>();
        private static List<Status> allStatus = new List<Status>();

        public Fangchance()
        {
            InitializeComponent();
        }

        private void Fangchance_Load(object sender, EventArgs e)
        {
            listPokemon = psrv.GetPokemon(true);
            allPokeBalls = pokeballService.GetPokeballs(true);

            foreach (Model.Pokemon pok in listPokemon)
            {
                cbPokemon.Items.Add(pok);
            }

            foreach (Model.Pokeball b in allPokeBalls)
            {
                cbBall.Items.Add(b);
            }

            cbBall.SelectedItem = allPokeBalls[0];

            allStatus.Add(new Status("", 1.0f));
            allStatus.Add(new Status("verbrannt", 1.5f));
            allStatus.Add(new Status("vergiftet", 1.5f));
            allStatus.Add(new Status("paralysiert", 1.5f));
            allStatus.Add(new Status("vereist", 2f));
            allStatus.Add(new Status("eingeschlafen", 2f));

            foreach (Status b in allStatus)
            {
                cbStatus.Items.Add(b);
            }
            cbStatus.SelectedItem = allStatus[0];

            cbPokemon.SelectedItem = listPokemon[0];

            tbGesundheit.Minimum = 1;
            tbGesundheit.Maximum = 100;
            tbGesundheit.Value = tbGesundheit.Maximum;

            lblErg.Text = "";
            txtProzent.Text = tbGesundheit.Value + "%";
        }

        void CalculateChance()
        {
            //http://www.bisaboard.de/index.php/Thread/204102-Pokemon-Fangrate-Formel-Frage/
            Model.Pokemon pok = (Model.Pokemon)cbPokemon.SelectedItem;
            Model.Pokeball pokBall = (Model.Pokeball)cbBall.SelectedItem;
            Status pokStatus = (Status)cbStatus.SelectedItem;

            float prozent = (float)tbGesundheit.Value / 100f;
            float fangrate = pok.Capture_Rate;
            float status = pokStatus.Rate;
            float ball = pokBall.CatchRate;

            float erg = ((3f - 2f * prozent) * fangrate * status * ball) / 3f;
            erg = erg * (100.0f / 255.0f);

            lblErg.Text = Math.Round(erg, 2).ToString() + "%";
        }

        private void tbGesundheit_Scroll(object sender, EventArgs e)
        {
            txtProzent.Text = tbGesundheit.Value + "%";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CalculateChance();
        }

        private void txtProzent_TextChanged(object sender, EventArgs e)
        {
            if (txtProzent.Text != "")
            {
                tbGesundheit.Value = Convert.ToInt32(Regex.Replace(txtProzent.Text, "[^.0-9]", ""));
            }
        }

        private void txtProzent_TextAlignChanged(object sender, EventArgs e)
        {

        }

        private void txtProzent_TabStopChanged(object sender, EventArgs e)
        {

        }
    }
}
