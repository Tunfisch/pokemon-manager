﻿using log4net;
using Pokemon_Manager.MultiLanguage;
using Pokemon_Manager.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pokemon_Manager.Forms
{
    public partial class Detail : Form
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IPokemonService psrv = new PokemonService();
        private int currentID;
        private Model.Pokemon currentPokemon;

        public Detail()
        {
            InitializeComponent();
            InitializeLocalisation();
            this.KeyPreview = true;
            currentID = 1;
            FillPokemonData();
        }

        private void InitializeLocalisation()
        {

        }

        public Detail(int getPokeID)
        {
            InitializeComponent();
            this.KeyPreview = true;
            currentID = getPokeID;
            FillPokemonData();
            lblName.Text = Language.test;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            currentID--;
            FillPokemonData();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            currentID++;
            FillPokemonData();
        }

        private void FillPokemonData()
        {
            currentPokemon = psrv.GetPokemonById(Convert.ToInt32(currentID), true)[0];
            lblId.Text = currentPokemon.Id.ToString();
            lblName.Text = currentPokemon.Name;
            lblTyp1.Text = currentPokemon.Typ1;
            lblTyp2.Text = currentPokemon.Typ2;
            chbCatched.Checked = currentPokemon.Catch;

            ReadPokemonImage(currentPokemon.Id);
        }

        private bool ReadPokemonImage(int pokeId)
        {
            string spId = pokeId.ToString();
            string pokefilepath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "pokemon_art" + Path.DirectorySeparatorChar + spId + ".png";

            if (File.Exists(pokefilepath))
            {
                try
                {
                    picPokemonImage.Image = Image.FromFile(pokefilepath);
                    return true;
                }
                catch(Exception ex)
                {
                    log.Error("Image konnte nicht in picbox geladen werden", ex);
                    return false;
                }
            }
            else
            {
                log.Warn("Pokemon sugimori art file doesnt exists");
                return false;
            }
        }

        private void chbCatched_CheckedChanged(object sender, EventArgs e)
        {
            currentPokemon.Catch = chbCatched.Checked;
            psrv.UpdatePokemon(currentPokemon);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Right)
            {
                currentID++;
                FillPokemonData();
            }
            else if (keyData == Keys.Left)
            {
                currentID--;
                FillPokemonData();
            }
            else if(keyData == Keys.F1)
            {
                chbCatched.Checked = !chbCatched.Checked;
                currentPokemon.Catch = chbCatched.Checked;
                psrv.UpdatePokemon(currentPokemon);
            }
            else if (keyData == Keys.Escape)
            {
                this.Close();
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
