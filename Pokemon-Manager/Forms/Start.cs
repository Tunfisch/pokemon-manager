﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Pokemon_Manager.Helper;
using Pokemon_Manager.Service;

namespace Pokemon_Manager.Forms
{
    public partial class Start : Form
    {
        private IPokemonService psrv = new PokemonService();
        private List<Model.Pokemon> currentPokemonList;
        private string testString;

        public Start()
        {
            InitializeComponent();
            testString = "";
            timer1.Interval = POStartIni.TimerValue;
        }

        private void Start_Load(object sender, EventArgs e)
        {
            LoadPokemonDataIntoTable();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            timer1.Start();

            if(keyData == Keys.F1)
            {
                if ((bool)dataGridView1.CurrentRow.Cells[4].Value == true)
                    dataGridView1.CurrentRow.Cells[4].Value = false;
                else
                    dataGridView1.CurrentRow.Cells[4].Value = true;
                    
                UpdatePokemon();
            }
            else if(keyData == Keys.F2)
            {
                using (Detail detail = new Detail(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value)))
                {
                    detail.ShowDialog();
                    LoadPokemonDataIntoTable();
                }
            }

            if (keyData == Keys.D0 || keyData == Keys.D1 || keyData == Keys.D2 || keyData == Keys.D3
                || keyData == Keys.D4 || keyData == Keys.D5 || keyData == Keys.D6
                || keyData == Keys.D7 || keyData == Keys.D8 || keyData == Keys.D9
                || keyData == Keys.NumPad0 || keyData == Keys.NumPad1
                || keyData == Keys.NumPad2 || keyData == Keys.NumPad3
                || keyData == Keys.NumPad4 || keyData == Keys.NumPad5
                || keyData == Keys.NumPad6 || keyData == Keys.NumPad7
                || keyData == Keys.NumPad8 || keyData == Keys.NumPad9)
            {
                string keyString = keyData.ToString();
                if(keyString.Contains("NumPad"))
                {
                    testString += keyString.Replace("NumPad", "");
                }
                else
                {
                    testString += keyString.Replace("D", "");
                }
                
                if(testString.Length > 3)
                {
                    if (keyString.Contains("NumPad"))
                    {
                        testString = keyString.Replace("NumPad", "");
                    }
                    else
                    {
                        testString = keyString.Replace("D", "");
                    }
                }

                if (Convert.ToInt32(testString) <= Model.Pokemon.MAXPOKEMON && Convert.ToInt32(testString) > 0)
                    dataGridView1.CurrentCell = dataGridView1.Rows[Convert.ToInt32(testString)-1].Cells[0];
            }

            if (keyData == Keys.Escape)
            {
                testString = "";
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            using (Detail detail = new Detail(Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value)))
            {
                detail.ShowDialog();
                LoadPokemonDataIntoTable();
            }
        }

        private void dataGridView1_CurrentCellDirtyStateChanged_1(object sender, EventArgs e)
        {
            if (dataGridView1.IsCurrentCellDirty)
            {
                dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            UpdatePokemon();
        }

        private void LoadPokemonDataIntoTable()
        {
            currentPokemonList = psrv.GetPokemon(true);
            dataGridView1.DataSource = psrv.GetDataTableFromPokemon(currentPokemonList);

            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                for (int j = 0; j < dataGridView1.Columns.Count - 1; ++j)
                {
                    dataGridView1.Rows[i].Cells[j].ReadOnly = true;
                }
            }
        }

        private void UpdatePokemon()
        {
            bool currentCellValue = Convert.ToBoolean(dataGridView1.CurrentRow.Cells[4].Value);
            int rpId = dataGridView1.CurrentCell.RowIndex;
            currentPokemonList[rpId].Catch = currentCellValue;
            psrv.UpdatePokemon(currentPokemonList[rpId]);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            testString = "";
            timer1.Stop();
        }

        private void tipsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using(Tips tips = new Tips())
            {
                tips.ShowDialog();
            }
        }

        private void fangchancenrechnerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using(Fangchance frm = new Fangchance())
            {
                frm.ShowDialog();
            }
        }
    }
}
