﻿using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Helper
{
    class POStartIni
    {
        private static FileIniDataParser parser;
        private static IniData data;
        private static readonly string filePath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "start.ini";

        static POStartIni()
        {
            parser = new FileIniDataParser();
            data = null;

            try
            {
                data = parser.ReadFile(filePath);
            }
            catch(Exception ex)
            {
                //log.Error("data.ini konnte nicht geparsed werden", ex);
            }
        }

        public static int TimerValue
        {
            get { return Convert.ToInt32(data["Start"]["timervalue"]); }
        }

        public static string getValue(string key, string subKey)
        {
            if (data[key][subKey] != null)
            {
                return data[key][subKey];
            }
            else
            {
                return "";
            }
        }
    }
}
