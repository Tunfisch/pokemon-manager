﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pokemon_Manager.Data;
using System.Data;

namespace Pokemon_Manager.Service
{
    public class PokemonService : IPokemonService
    {
        private IPokemonRepository rep;

        public PokemonService()
        {
            rep = DatabaseRepository.GetPokemonRepository();
        }

        public List<Model.Pokemon> GetPokemon(bool isAuthenticated)
        {
            if(isAuthenticated)
            {
                return rep.GetAllPokemon();
            }
            else
            {
                return null;
            }
        }

        public List<Model.Pokemon> GetPokemonById(int id, bool isAuthenticated)
        {
            if (isAuthenticated)
            {
                List<Model.Pokemon> pokeList = rep.GetPokemonById(id);

                if (pokeList.Count > 0)
                {
                    return pokeList;
                }
                else
                {
                    List<Model.Pokemon> l = new List<Model.Pokemon>();
                    l.Add(new Model.Pokemon());
                    return l;
                }
            }
            else
            {
                return null;
            }            
        }

        public List<Model.Pokemon> GetPokemonLikeIdentifier(string identifier, bool isAuthenticated)
        {
            if (isAuthenticated)
            {
                return rep.GetPokemonLikeIdentifier(identifier);
            }
            else
            {
                return null;
            }
        }

        public List<Model.Pokemon> GetPokemonByIdAndIdentifier(string identifier, int id, bool isAuthenticated)
        {
            if (isAuthenticated)
            {
                return rep.GetPokemonLikeIdentifierAndId(identifier, id);
            }
            else
            {
                return null;
            }
        }

        public DataTable GetDataTableFromPokemon(List<Model.Pokemon> pokemonList)
        {
            return rep.GetDataTable(pokemonList);
        }

        public void UpdatePokemon(Model.Pokemon pokemon)
        {
            rep.UpdatePokemon(pokemon);
        }
    }
}
