﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Service
{
    public interface IPokemonService
    {
        List<Model.Pokemon> GetPokemon(bool isAuthenticated);
        List<Model.Pokemon> GetPokemonById(int id, bool isAuthenticated);
        List<Model.Pokemon> GetPokemonLikeIdentifier(string identifier, bool isAuthenticated);
        List<Model.Pokemon> GetPokemonByIdAndIdentifier(string identifier, int id, bool isAuthenticated);
        DataTable GetDataTableFromPokemon(List<Model.Pokemon> pokemonList);
        void UpdatePokemon(Model.Pokemon pokemon);
    }
}
