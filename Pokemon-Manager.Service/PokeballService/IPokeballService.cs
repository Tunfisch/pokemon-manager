﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Service
{
    public interface IPokeballService
    {
        List<Model.Pokeball> GetPokeballs(bool isAuthenticated);
    }
}
