﻿using Pokemon_Manager.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Service
{
    public class PokeballService : IPokeballService
    {
        private IPokeballRepository ipkb;

        public PokeballService()
        {
            ipkb = DatabaseRepository.GetPokeballRepository();
        }

        public List<Model.Pokeball> GetPokeballs(bool isAuthenticated)
        {
            if (isAuthenticated)
            {
                return ipkb.GetAllPokeballs();
            }
            else
            {
                return null;
            }
        }
    }
}
