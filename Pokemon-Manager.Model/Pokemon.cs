﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Model
{
    public class Pokemon
    {
        public static readonly int MAXPOKEMON = 721;
        private int pokemon_id;
        private int pokemon_base_experience;
        private int pokemon_height;
        private int pokemon_weight;
        private string pokemon_name;
        private string pokemon_typ1;
        private string pokemon_typ2;
        private int pokemon_capture_rate;
        private bool pokemon_catch;

        private int pokemon_status_kp;
        private int pokemon_status_attack;
        private int pokemon_status_defense;
        private int pokemon_status_special_attack;
        private int pokemon_status_special_defense;
        private int pokemon_status_initiative;

        public int Id { get { return pokemon_id; } set { pokemon_id = value; } }
        public int Base_Experience { get { return pokemon_base_experience; } set { pokemon_base_experience = value; } }
        public int Height { get { return pokemon_height; } set { pokemon_height = value; } }
        public int Weight { get { return pokemon_weight; } set { pokemon_weight = value; } }
        public int Capture_Rate { get { return pokemon_capture_rate; } set { pokemon_capture_rate = value; } }
        public string Name { get { return pokemon_name; } set { pokemon_name = value; } }
        public string Typ1 { get { return pokemon_typ1; } set { pokemon_typ1 = value; } }
        public string Typ2 { get { return pokemon_typ2; } set { pokemon_typ2 = value; } }
        public bool Catch { get { return pokemon_catch; } set { pokemon_catch = value; } }
        
        public int Status_KP { get { return pokemon_status_kp; } set { pokemon_status_kp = value; } }
        public int Status_Attack { get { return pokemon_status_attack; } set { pokemon_status_attack = value; } }
        public int Status_Defense { get { return pokemon_status_defense; } set { pokemon_status_defense = value; } }
        public int Status_Special_Attack { get { return pokemon_status_special_attack; } set { pokemon_status_special_attack = value; } }
        public int Status_Special_Defense { get { return pokemon_status_special_defense; } set { pokemon_status_special_defense = value; } }
        public int Status_Initiative { get { return pokemon_status_initiative; } set { pokemon_status_initiative = value; } }
        
        public Pokemon()
        {
            pokemon_id = 0;
            pokemon_base_experience = 0;
            pokemon_height = 0;
            pokemon_weight = 0;
            pokemon_capture_rate = 1;
            pokemon_name = "";
            pokemon_typ1 = "";
            pokemon_typ2 = "";
            pokemon_catch = false;

            pokemon_status_kp = 0;
            pokemon_status_attack = 0;
            pokemon_status_defense = 0;
            pokemon_status_special_attack = 0;
            pokemon_status_special_defense = 0;
            pokemon_status_initiative = 0;
        }

        public Pokemon(int id, string name, string typ1, string typ2, bool catched, int capture_rate)
        {
            SetStandard(id, name, typ1, typ2, catched, capture_rate);
        }

        private void SetStandard(int id, string name, string typ1, string typ2, bool catched, int capture_rate)
        {
            Id = id;
            Name = name;
            Typ1 = typ1;
            Typ2 = typ2;
            Catch = catched;
            Capture_Rate = capture_rate;
        }

        private void SetStatusValues()
        {

        }

        public override string ToString()
        {
            return this.pokemon_name;
        }
    }
}
