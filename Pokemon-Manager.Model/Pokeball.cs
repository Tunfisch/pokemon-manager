﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pokemon_Manager.Model
{
    public class Pokeball
    {
        private string ball_name;
        private float ball_catchrate;

        public string Name { get { return ball_name; } set { ball_name = value; } }
        public float CatchRate { get { return ball_catchrate; } set { ball_catchrate = value; } }

        public Pokeball(string name, float catchrate)
        {
            ball_name = name;
            ball_catchrate = catchrate;
        }

        public override string ToString()
        {
            return ball_name;
        }
    }
}
